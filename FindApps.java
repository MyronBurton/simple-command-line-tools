import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FindApps {

	public static void main(String[] args) {

		if (args.length != 1) {
			System.out.println("Please pass in the filepath as the only argument");
			return;
		}
		
		Scanner in;
		try {
			in = new Scanner(new File(args[0]));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		}
		
		String first = "\\\\([A-Za-z0-9-._+]*)";
		String second = "\\.exe";
		String pattern = first + second;
		Pattern exeMatch;
		String matchedApps = "";
		int i = 1;
		while (in.hasNextLine()) {
			exeMatch = Pattern.compile(pattern);
			String line = in.nextLine();
			Matcher matcher = exeMatch.matcher(line);
			if (matcher.find()) {
//				System.out.print("New exe found first on line " + i + ": ");
				System.out.println(matcher.group().substring(1, matcher.group().length()));
				matchedApps = "(?<!\\Q" + matcher.group().substring(1, matcher.group().length() - 4) + "\\E)" + matchedApps;
				pattern = first + matchedApps + second;
			}
			i++;
		}
		in.close();
	}

}

